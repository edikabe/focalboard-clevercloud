FROM ubuntu:20.04

ARG APP_HOST_ROOT
ARG APP_PORT
ARG DB_URI
ARG DB_NAME

ENV APP_PORT_HOST=$APP_PORT_HOST
ENV APP_PORT=$APP_PORT
ENV DB_URI=$DB_URI
ENV DB_NAME=$DB_NAME

# Make sure that the underlying container is patched to the latest versions
RUN apt-get update && \
    apt-get install -y gzip tar wget gettext-base 

# Now install Focalboard as a seperate layer
RUN wget https://github.com/mattermost/focalboard/releases/download/v0.6.1/focalboard-server-linux-amd64.tar.gz && \
    tar -xvzf focalboard-server-linux-amd64.tar.gz && \
    mv focalboard /opt

WORKDIR /opt/focalboard

COPY config.json.tpl config.json.tpl
COPY create_config_file_from_env.sh create_config_file_from_env.sh

RUN ./create_config_file_from_env.sh

EXPOSE 8000

CMD /opt/focalboard/bin/focalboard-server
