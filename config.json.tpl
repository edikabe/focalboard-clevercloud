{
    "serverRoot": "$APP_HOST_ROOT",
    "port": $APP_PORT,
    "dbtype": "postgres",
    "dbconfig": "$DB_URI",
    "postgres_dbconfig": "dbname=$DB_NAME sslmode=disable",
    "useSSL": false,
    "webpath": "./pack",
    "filespath": "./files",
    "telemetry": true,
    "session_expire_time": 2592000,
    "session_refresh_time": 18000,
    "localOnly": false,
    "enableLocalMode": true,
    "localModeSocketLocation": "/var/tmp/focalboard_local.socket"
}
